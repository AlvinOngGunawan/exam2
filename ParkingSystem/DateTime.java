package com.nexsoft.ParkingSystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateTime extends Vehicle {

    DateTimeFormatter newParser = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
    String out = "2020-06-23 10:10:10";
    LocalDate now = LocalDate.now();

    Date date1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(out);
    ZoneId zona = ZoneId.systemDefault();
    Instant instan = date1.toInstant();
    LocalDate date2 = instan.atZone(zona).toLocalDate();

    long operator = ChronoUnit.HOURS.between(now, date2);

    public void setOut(String out) {
        this.out = out;
    }

    public LocalDate getNow() {
        return now;
    }

    public DateTime() throws ParseException {
    }
}
